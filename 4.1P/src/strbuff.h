/* strbuff.h -- Header file for strBuff class
   
   This is a dynamically resizing string buffer.  Output is written to
   an external buffer, and then appended to the internal buffer.
   Internal state maintains a pointer to the current end of the
   string.  If the buffer to be appended would cause an overflow of
   the internal buffer, the internal string is doubled in size before
   the copy.

   Based on an idea by Jeremy Buhler (jbuhler@rice.edu). */
   
#if !defined(STRBUFF_H)
#define STRBUFF_H

#include <stdio.h>
#include <string.h>

typedef struct {
  char* string;       /* internal string */
  int size;           /* current size of internal buffer */
  int stindex;          /* current index to end of string */
} strBuff;

strBuff* newStrBuff();
#if defined(KNR_PROTO)
void append();
#else
void append(strBuff*, char*);
#endif /* defined(KNR_PROTO) */

#endif  /* if !defined(STRBUFF_H) */
