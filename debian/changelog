fastlink (4.1P-fix100+dfsg-4) unstable; urgency=medium

  [ Shruti Sridhar ]
  * Team upload.
  * Add autopkgtests. Closes: #909713
  * Add docs
  * Add data
  * Add examples

  [ Andreas Tille ]
  * Standards-Version: 4.5.1

 -- Shruti Sridhar <shruti.sridhar99@gmail.com>  Mon, 22 Feb 2021 01:30:24 +0530

fastlink (4.1P-fix100+dfsg-3) unstable; urgency=medium

  * Add fake watch file
  * debian/get-orig-source: Do not create new tarball if there is no new
    patchlevel
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target (routine-
    update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Refer to specific version of license GPL-2+.
  * Use -fcommon option to tolerate "multiple definition of"
    Closes: #957194
  * Propagate CFLAGS set by Debian to all binaries

 -- Andreas Tille <tille@debian.org>  Fri, 30 Oct 2020 10:02:10 +0100

fastlink (4.1P-fix100+dfsg-2) unstable; urgency=medium

  * Secure URI in Homepage
  * debian/get-orig-source: use https instead of ftp
  * d/rules: Be more cross-building friendly
  * hardening=+all
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Thu, 05 Jul 2018 11:35:01 +0200

fastlink (4.1P-fix100+dfsg-1) unstable; urgency=low

  * New upstream patch level
  * debian/upstream/metadata
  * debhelper 9
  * cme fix dpkg-control
  * d/rules: short dh
  * DEP5
  * Moved packaging from SVN to Git
  * Provide fake d/watch to document that upstream has no versioned tarball
  * Propagate hardening flags

 -- Andreas Tille <tille@debian.org>  Sun, 10 Jan 2016 10:17:50 +0100

fastlink (4.1P-fix95-3) unstable; urgency=low

  * Replaced debian/*.desktop by debian/menu to make sure users of
    modern desktop systems using freedesktop.org standards will not
    be confused by a missing graphical interface
    Closes: #639998
  * debian/control:
    - Standards-Version: 3.9.2 (no changes needed)
    - Fixed Vcs fields
    - Dropped article in short description
  * Debhelper 8 (control+compat)

 -- Andreas Tille <tille@debian.org>  Mon, 19 Dec 2011 10:34:20 +0100

fastlink (4.1P-fix95-2) unstable; urgency=low

  * debian/copyright: Fixed source download location
  * debian/control:
    - Standards-Version: 3.9.1
    - Fix spelling of Debian Med
    - debhelper (>= 7)
    - Depends: ${misc:Depends}
  * debian/compat: 7
  * debian/watch: use GZIP="--best --no-name" to make sure the
    resulting tarball will be identically
  * debian/source/format: 3.0 (quilt)
  * debian/rules: remove dh_desktop call

 -- Andreas Tille <tille@debian.org>  Fri, 14 Jan 2011 08:40:14 +0100

fastlink (4.1P-fix95-1) unstable; urgency=low

  * New upstream bugfix release
  * debian/control: Better description
  * Standards-Version: 3.8.0 (no changes needed)
  * Removed explicite versioning from cdbs and debhelper
  * Machine readable copyright files
  * debian/loadscore.1: s?\.5?1/2? because man treated .5 as control
  * gcc3.3 patch is not needd any more - so neither the quilt
    dependency is needed

 -- Andreas Tille <tille@debian.org>  Tue, 09 Dec 2008 07:40:11 +0100

fastlink (4.1P-fix94b-2) unstable; urgency=low

  * Changed the doc-base section according to the new policy.
  * Added myself to the `Uploaders' field in debian/control.

 -- Charles Plessy <plessy@debian.org>  Sun, 06 Jul 2008 22:49:23 +0900

fastlink (4.1P-fix94b-1) unstable; urgency=low

  * New upstream patch
  * Don't autogenerate debian/control, remove debian/control.in
  * Build-Depends: quilt
  * Standards-Version: 3.7.3 (No changes needed)
  * debian/control: XCBS-URL -> Homepage
  * Removed XB-Tag (isn't used) and Homepage from long description
  * Removed [Biology] from short description
  * Group maintenance
    - Maintainer: Debian-Med Packaging Team
      <debian-med-packaging@lists.alioth.debian.org>
    - DM-Upload-Allowed: yes
    - Uploaders: Andreas Tille <tille@debian.org>
    - Vcs-Browser, Vcs-Svn
  * Doc package to section doc

 -- Andreas Tille <tille@debian.org>  Mon, 18 Feb 2008 17:54:17 +0100

fastlink (4.1P-fix92-1) unstable; urgency=low

  * New upstream fix package
  * Standards-Version: 3.7.2 (no changes necessary)
  * Switched to cdbs
  * Setting DebTags
  * Added desktop files

 -- Andreas Tille <tille@debian.org>  Sat,  9 Sep 2006 12:52:33 +0200

fastlink (4.1P-fix91-1) unstable; urgency=low

  * New bugfix release
  * removed some files which are very specific for running fastlink on
    AIX, VMS and IRIX from source package which had to be repackaged anyway.
  * Standards-Version: 3.6.1.1

 -- Andreas Tille <tille@debian.org>  Wed,  1 Dec 2004 14:19:29 +0100

fastlink (4.1P-fix88-3) unstable; urgency=low

  * Fixed maintainer address in debian/control
    closes: #195322
  * Patched 4.1P/src/{commondefs,unknown}.h to replace varargs.h by stdarg.h
    Ported functions in iostuff.c to get rid of va_* stuff.  Many thanks to
    Colin Watson <cjwatson@debian.org> for his patches.
    Replaced <sys/time.h> by <time.h>
    These points fix FTBFS with gcc-3.3.
    closes: #194913

 -- Andreas Tille <tille@debian.org>  Mon,  2 Jun 2003 09:27:22 +0200

fastlink (4.1P-fix88-2) unstable; urgency=low

  * What difference does it make to the dead, the orphans and the homeless,
    whether the mad destruction is wrought under the name of totalitarianism
    or the holy name of liberty or democracy?
        -- Mahatma Gandhi (1869 - 1948), "Non-Violence in Peace and War"
  * Added manpages for data formats and replaced unknown.1.  These
    pages were kindly provided by Elizabeth Barham
    <lizzy@soggytrousers.net>
  * fastlink-doc now only suggests fastlink
  * Added doc-base entries.
  * CHanged compile time option from -g to -Wall

 -- Andreas Tille <tille@debian.org>  Sat, 26 Apr 2003 13:50:38 +0200

fastlink (4.1P-fix88-1) unstable; urgency=low

  * The "I'm against war" release
    Mankind must put an end to war before war puts an end to mankind.
        -- John F. Kennedy
  * New upstream release (bugfixes)
  * New maintainer
    closes: #100221
  * Build-Depends: debhelper (>= 4.0)
  * Standards-Version: 3.5.9
  * debian/compat now stores debhelper compatibility version
  * Compliant to developers reference "6.3.2. Upstream home page" and
    XBCS-URL: http://www.ncbi.nlm.nih.gov/CBBResearch/Schaffer/fastlink.html
  * Updated fastlink.html from web page
  * Wrote manpages.
  * Removed the following files from binary package because they just describe
    Infromation for compilation and installation:
    README.AIX README.ALPHAVMS README.DOS README.Digital README.IRIX README.VAX
    README.VMS README.djgpp README.install README.makefile README.portability
  * Split up into two packages to remove papers from the binary package.
    This reduces space on Debian mirrors because the main part of the
    binary package was architecture "all" and probably saves disk space for
    local installs were these docs are not needed.

 -- Andreas Tille <tille@debian.org>  Tue, 15 Apr 2003 09:51:31 +0200

fastlink (4.1P-fix81-2) unstable; urgency=low

  * Maintainer set to Debian QA Group <packages@qa.debian.org>.

 -- Adrian Bunk <bunk@fs.tum.de>  Fri, 24 Aug 2001 23:06:48 +0200

fastlink (4.1P-fix81-1) unstable; urgency=low

  * New upstream release (upstream used the same version number 4.1P, but
    introduced some new bugfixes incl. 78-81); closes: #43471
  * Enclosed the fastlink homepage to docs, and a README.Debian.
  * Updated Debian package internals.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Sat, 12 May 2001 18:43:26 +0200

fastlink (4.1P-2) unstable; urgency=low

  * Adopted by new maintainer; closes: #92804
  * Removed several unnecessary files in debian subdirectory.
  * Updated to latest standards version and added Build-Depends (changed
    control, copyright, dirs, docs, and rules);
    closes: #91154, #91441, #42712
  * Moved package from misc to section science, because it is a tool that
    is exclusively useful for biologists.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Sat, 21 Apr 2001 09:17:46 +0200

fastlink (4.1P-1) unstable; urgency=low

  * New upstream release

 -- Stephane Bortzmeyer <bortzmeyer@debian.org>  Mon, 19 Jul 1999 17:39:24 +0200

fastlink (4.0P-1) unstable; urgency=low

  * Initial Release.

 -- Stephane Bortzmeyer <bortzmeyer@debian.org>  Fri,  5 Feb 1999 15:36:21 +0100

Local variables:
mode: debian-changelog
End:
